import React, { useEffect, useMemo } from 'react';
import {
  View,
  Alert
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { useSelector, useDispatch, } from "react-redux";
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator()

import { AuthContext } from './src/utils/context';
import { axiop } from './src/utils/fetch';
import SignStack from './src/Routers/SignInStack';
import MainStack from './src/Routers/MainStack';
import MainTabStack from './src/Routers/MainTabStack';
import MainTab from './src/BottomTab/MainTab';
import LoadingModal from './src/View/LoadingModal/LoadingModal';

const App = (props) => {

  const store = useSelector((state) => state)
  const dispatch = useDispatch()

  useEffect(
    () => {
      console.log('Token ', store.signIn.access_token)
    }, [store.signIn.access_token]
  )

  const authContext = useMemo(
    () => ({
      signIn: async (user, pass) => {
        console.log('signIn ', user)
        dispatch({ type: 'utils/loading', isLoading: true })
        let options = { url: 'login', method: 'post', data: { user_id: user, password: pass }, }
        const _data = await axiop(options)
        console.log('signIn _data ', _data)
        if (_data.fetchCheck) {
          dispatch({ type: 'signIn/userSignIn', ..._data.data })
        } else {
          Alert.alert(_data.message)
        }
        setTimeout(() => {
          dispatch({ type: 'utils/loading', isLoading: false })
        }, 1000);
      },
      signOut: async () => {
        dispatch({ type: 'signOut/signOut' })
      }
    })
  )

  // const TabView = () => {
  //   return (
  //     <MainTab />
  //   )
  // }

  if (store.signIn.access_token) {
    // if (1===1) {
    return (
      <AuthContext.Provider value={authContext} >
        <NavigationContainer>
          <MainTabStack />
          {/* <MainStack /> */}
          {/* <MainTab /> */}
          {/* {TabView()} */}
          {/* <Stack.Navigator>
          <Stack.Screen name='MainTab' component={TabView} options={{headerShown: false}} />
          </Stack.Navigator> */}
          <LoadingModal isVisible={store.utils.isLoading} />
        </NavigationContainer>
      </AuthContext.Provider>
    )
  } else {
    return (
      <AuthContext.Provider value={authContext} >
        <NavigationContainer>
          <SignStack />
        </NavigationContainer>
      </AuthContext.Provider>
    )
  }


}

export default App


