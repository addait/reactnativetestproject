const initialState = {
    access_token: "",
    user_id: "ADMIN",
    user_level: "A",
    full_name: "โปรแกรมเมอร์",
    position: "โปรแกรมเมอร์",
    department_code: "20001",
    department_name: "IT",
}

const signInReducer = (state = initialState, { type, ...action }) => {
    switch (type) {
        case 'signIn/userSignIn':
            return {
                ...state,
                access_token: action.access_token,
                user_id: action.user_id,
                user_level: action.user_level,
                full_name: action.position,
                department_code: action.department_code,
                department_name: action.department_name,
            }
        case 'signIn/signOut':
            return {
                ...state,
                access_token: '',
                user_id: '',
                user_level: '',
                full_name: '',
                department_code: '',
                department_name: '',
            }
        default:
            return state
    }
}

export default signInReducer;