import { createStore, combineReducers } from 'redux'
import signInReducer from './signInReducer'
import utilsReducer from './utilsReducer'

const rootReducer = combineReducers({
    signIn: signInReducer,
    utils: utilsReducer,
})

const store = createStore(rootReducer)
export default store