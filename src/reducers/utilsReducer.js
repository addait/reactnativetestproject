const initialState = {
    isLoading: false,
}

const utilsReducer = (state = initialState, { type, ...action }) => {
    switch (type) {
        case 'utils/loading':
            return {
                ...state,
                isLoading: action.isLoading,
            }
        default:
            return state
    }
}

export default utilsReducer;