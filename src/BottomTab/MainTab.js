import React from 'react'
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { StyleSheet } from 'react-native'
import { useSelector, useDispatch } from "react-redux"

import License from '../View/License/License'
import Product from '../View/Product/Product'
import MainStack from '../Routers/MainStack'
// import { View } from 'react-native-animatable'

const Tab = createBottomTabNavigator()

const MainTab = ({ navigation }) => {

    const store = useSelector((state) => state)

    // const ProductView = (props) => <Product {...props} />
    // const LicenseView = (props) => <License {...props} />

    return (
        <Tab.Navigator
            detachInactiveScreens={false}
            initialRouteName="product"
            options={{
                tabBarShowLabel: true,
                tabBarLabel: 'Article',
                tabBarIcon: 'file-document',
                tabBarColor: '#C9E7F8',
            }}
        >
            <Tab.Screen
                name="product"
                component={Product}
                options={{
                    title: "Product",
                    headerShown: false,
                    tabBarShowLabel: true,
                    tabBarLabel: "Product List",
                    tabBarColor: '#4C9900',
                    tabBarIcon: ({ color }) => (
                        <Icon name="newspaper-outline" color={color} size={26} />
                    ),
                    // tabBarBadge: store.utils.isLoading? true : false,
                }}
                // listeners={{
                //     tabPress: e => {
                //         navigation.reset({
                //             index: 0,
                //             routes: [{ name: 'product' }],
                //         })
                //     },
                // }}
            />
            <Tab.Screen
                name="license"
                component={License}
                options={{
                    title: "License",
                    headerShown: false,
                    tabBarShowLabel: true,
                    tabBarLabel: "License",
                    tabBarColor: '#6A7400',
                    tabBarIcon: ({ color }) => (
                        <Icon name="newspaper-outline" color={color} size={26} />
                    ),
                    tabBarBadgeStyle: {
                        size: 20
                    }
                }}
                // listeners={{
                //     tabPress: e => {
                //         navigation.reset({
                //             index: 0,
                //             routes: [{ name: 'license' }],
                //         })
                //     },
                // }}
            />
        </Tab.Navigator>
    )
}

export default MainTab
