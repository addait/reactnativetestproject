import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import SignIn from '../View/SignIn/SignIn'

const Stack = createStackNavigator()

const SignStack = () => {
    const SignView = (props) => <SignIn {...props} />

    return(
    <Stack.Navigator>
        <Stack.Screen name='Home' component={SignView} options={{headerShown: false}} />
    </Stack.Navigator>
    )
}

export default SignStack
