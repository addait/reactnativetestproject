import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Home from '../View/Home/Home'
import Page2 from '../View/Page2/Page2'
import Page3 from '../View/Page3/Page3'
import TestStack from './TetsStack'
import MainTab from '../BottomTab/MainTab'
import MainStack from './MainStack'

const Stack = createStackNavigator()

const MainTabStack = () => {

    return(
    <Stack.Navigator >
        <Stack.Screen name='MainTab' component={MainTab} options={{ headerShown: false, }}/>
        <Stack.Screen name='HomeStack' component={MainStack}  options={{ headerShown: false, }}/>
    </Stack.Navigator>
    // <Stack.Navigator
    // options={{
    //     headerShown: false,
    //   }}
    // >
    //     <Stack.Screen name='MainTab' component={MainTab} />
    // </Stack.Navigator>
    )
}

export default MainTabStack
