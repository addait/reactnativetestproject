import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Test from '../View/Test/Test'
import TestPage2 from '../View/TestPage2/TestPage2'

const Stack = createStackNavigator()

const TestStack = () => {
    const TestView = (props) => <Test {...props} />
    const TestPage2View = (props) => <TestPage2 {...props} />
    return(
    <Stack.Navigator>
        <Stack.Screen name='Test' component={TestView} />
        <Stack.Screen name='TestPage2' component={TestPage2View} />
    </Stack.Navigator>
    )
}

export default TestStack
