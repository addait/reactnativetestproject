import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Home from '../View/Home/Home'
import Page2 from '../View/Page2/Page2'
import Page3 from '../View/Page3/Page3'
import TestStack from './TetsStack'
import MainTab from '../BottomTab/MainTab'

const Stack = createStackNavigator()

const MainStack = () => {
    const HomeView = (props) => <Home {...props} />
    const Page2View = (props) => <Page2 {...props} />
    const Page3View = (props) => <Page3 {...props} />
    const TestStackView = (props) => <TestStack {...props} />
    const MainTabView = (props) => <MainTab {...props} />

    return(
    <Stack.Navigator >
        <Stack.Screen name='Home' component={HomeView} />
        <Stack.Screen name='Page2' component={Page2View} />
        <Stack.Screen name='Page3' component={Page3View} />
        <Stack.Screen name='TestView' component={TestStackView} />
    </Stack.Navigator>
    // <Stack.Navigator
    // options={{
    //     headerShown: false,
    //   }}
    // >
    //     <Stack.Screen name='MainTab' component={MainTab} />
    // </Stack.Navigator>
    )
}

export default MainStack
