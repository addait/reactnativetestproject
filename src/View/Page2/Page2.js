import React, {useEffect, useState} from "react";
import{
View,
Text,
TouchableOpacity,
Dimensions,
} from "react-native";
import { Card } from 'react-native-paper';
import { shadow } from "../../utils/shadow";

const { width, height } = Dimensions.get("window")

const Page2 = (props) => {

    const [fromPage, setFromPage] = useState('')
    const [value, setValue] = useState('')

    useEffect(
        () => {
            console.log('Page2 ',props)
            if(props.route.params !== undefined){
                setFromPage(props.route.params.page)
                setValue(props.route.params.value)
            }
        }, []
    )

    return(
        <View>
            <Text>
                Page2
            </Text>
            <Text>
                From: {fromPage}
            </Text>
            <Text>
                Value: {value}
            </Text>
            <Card
                style={{ height: height * 0.05, width: '50%', alignSelf: 'center', marginTop: 20, ...shadow.shadow10 }}
            >
                <TouchableOpacity
                    style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                    onPress={
                        () => {
                            props.navigation.goBack()
                        }
                    }
                >
                    <Text style={{ textAlign: 'center' }} >Go To Home</Text>
                </TouchableOpacity>
            </Card>
        </View>
    )
}

export default Page2