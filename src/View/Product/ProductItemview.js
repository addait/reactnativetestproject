import React, { useState } from 'react'
import { useSelector, useDispatch, } from "react-redux"
import {
    Text,
    View,
    Image,
    TouchableOpacity,
} from "react-native"

const ProductItemView = React.memo(({ item, index, }) => {

    const view = () => {

        return (
            <View style={{borderWidth: 1, marginBottom: 0,}}>
                <Text>
                    {item.product_id}
                </Text>
                <Text>
                    {item.product_name}
                </Text>
                <Text>
                    {item.license_name}
                </Text>
            </View>
        )
    }

    return (
        view()
    )
})

export default ProductItemView