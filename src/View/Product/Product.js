import React, { useEffect, useState, useContext, } from 'react'
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
} from 'react-native'
import { useSelector, useDispatch } from "react-redux"
import Spinner from 'react-native-loading-spinner-overlay/lib';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ProductItemView from './ProductItemview';
import { axiop } from '../../utils/fetch';

const Product = (props) => {

    const store = useSelector((state) => state)
    const dispatch = useDispatch()
    
    const [data, setData] = useState([])
    const [dataMaster, setDataMaster] = useState([])
    const [search, setSearch] = useState('')

    useEffect(
        () => {
            loadData()
        }, [props]
    )


    const loadData = async () => {
        dispatch({ type: 'utils/loading', isLoading: true })
        let options = { url: 'product', method: 'get', auth: `${store.signIn.user_id}:${store.signIn.access_token}` }
        const _data = await axiop(options)
        console.log('loadData Product ',)
        if(_data.fetchCheck && _data.data !== undefined){
            setData(_data.data)
            setDataMaster(_data.data)
        }
        setTimeout(() => {
            dispatch({ type: 'utils/loading', isLoading: false })
        }, 1000);
    }


    const textInputChange = (txt, setTxt) => {
        setTxt(txt)
        const results = dataMaster.filter(item => `${item.product_id}${item.product_name}${item.license_name}`.toUpperCase().includes(txt.toUpperCase()))
        console.log('textInput ', txt)
        setData(results)
    }

    const _rowRenderer = ({ item, index }) => {
        return (
            <ProductItemView item={item} index={index} />
        )
    }

    return (
        <View>
            <Text>Product</Text>
            <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <FontAwesome
                        name="search"
                        color='#000'
                        size={20}
                        style={{
                            margin: 5,
                        }}
                    />
                    <TextInput
                        placeholder={'ค้นหา'}
                        placeholderTextColor="#666666"
                        value={search}
                        style={{
                            borderRadius: 5,
                            backgroundColor: '#999',
                            color: '#000',
                            width: '80%',
                        }}
                        autoCapitalize="none"
                        onChangeText={(val) => textInputChange(val, setSearch)}
                    />
                </View>
            {
                data.length > 0 ?
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        data={data}
                        renderItem={_rowRenderer}
                        keyExtractor={(item, index) => `${index}`}
                        style={{
                            marginTop: 20,
                            width: '90%',
                            alignSelf: 'center',
                            borderWidth: 0,
                        }}
                    />
                    :
                    <Text style={{ textAlign: 'center' }} >{'ไม่พบข้อมูล'}</Text>
            }
        </View>
    )

}

export default Product