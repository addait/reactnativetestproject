import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import { Card } from 'react-native-paper';
import { shadow } from "../../utils/shadow";

const { width, height } = Dimensions.get("window")

const Home = (props) => {

    useEffect(
        () => {
            console.log('Home ', props)
        }, []
    )

    return (
        <View>
            <Text>
                Home
            </Text>
            <Card
                style={{ height: height * 0.05, width: '50%', alignSelf: 'center', ...shadow.shadow10 }}
            >
                <TouchableOpacity
                    style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                    onPress={
                        () => {
                            props.navigation.navigate('Page2', { page: 'Home', value: 'Test from Home' })
                        }
                    }
                >
                    <Text style={{ textAlign: 'center' }} >Go To Page2</Text>
                </TouchableOpacity>
            </Card>
            <Card
                style={{ height: height * 0.05, width: '50%', alignSelf: 'center', marginTop: 20, ...shadow.shadow10 }}
            >
                <TouchableOpacity
                    style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                    onPress={
                        () => {
                            props.navigation.reset({
                                index: 0,
                                routes: [{ name: 'Page3', params: {page: 'Home', value: 'Test from Home'} }]
                            })
                        }
                    }
                >
                    <Text style={{ textAlign: 'center' }} >Go To Page3</Text>
                </TouchableOpacity>
            </Card>
            <Card
                style={{ height: height * 0.05, width: '50%', alignSelf: 'center', marginTop: 20, ...shadow.shadow10 }}
            >
                <TouchableOpacity
                    style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                    onPress={
                        () => {
                            props.navigation.navigate('TestView', { page: 'Home', value: 'Test from Home' })
                        }
                    }
                >
                    <Text style={{ textAlign: 'center' }} >Go To TestStack</Text>
                </TouchableOpacity>
            </Card>
            <Card
                style={{ height: height * 0.05, width: '50%', alignSelf: 'center', marginTop: 20, ...shadow.shadow10 }}
            >
                <TouchableOpacity
                    style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                    onPress={
                        () => {
                            props.navigation.reset({
                                index: 0,
                                routes: [{ name: 'MainTab', params: {page: 'Home', value: 'Test from Home'} }]
                            })
                        }
                    }
                >
                    <Text style={{ textAlign: 'center' }} >Go To MainTab</Text>
                </TouchableOpacity>
            </Card>
        </View>
    )

}

export default Home