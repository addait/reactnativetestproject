import React, { useEffect, useState, useContext, } from 'react'
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
} from 'react-native'
import * as Animatable from 'react-native-animatable'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
// import { useSelector, useDispatch } from "react-redux"
import Spinner from 'react-native-loading-spinner-overlay/lib';
import { Card } from 'react-native-paper'

import { AuthContext } from '../../utils/context'
import { shadow } from "../../utils/shadow";

const { width, height } = Dimensions.get("window")

const SignIn = (props) => {

    const { signIn } = useContext(AuthContext)

    const [userID, setUserID] = useState('')
    const [pass, setPass] = useState('')
    const [isValidUser, setIsValidUser] = useState(false)
    const [isValidPass, setIsValidPass] = useState(false)

    const textInputChange = (txt, setTxt, setValid) => {
        setTxt(txt)
        if(txt !== ''){
            setValid(false)
        }else{
            setValid(true)
        }
    }

    const handleSign = () => {
        if(userID === ''){
            setIsValidUser(true)
        }else if (pass === ''){
            setIsValidPass(true)
        }else {
            signIn(userID, pass)
        }
    }

    return (
        <View style={[
            {
                flex: 1,
                justifyContent: 'center',
            }
        ]}>
            <Card
                style={{
                    width: '80%',
                    alignSelf: 'center'
                }}
            >
                <View
                    style={{
                        padding: 10
                    }}
                >
                    <Text style={{
                        color: 'black',
                    }}>{'Test Project'}</Text>
                </View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <FontAwesome
                        name="user-o"
                        color='#000'
                        size={20}
                        style={{
                            margin: 5,
                        }}
                    />
                    <TextInput
                        placeholder={'ชื่อผู้ใช้'}
                        placeholderTextColor="#666666"
                        value={userID}
                        style={{
                            borderRadius: 5,
                            backgroundColor: '#999',
                            color: '#000',
                            width: '80%',
                        }}
                        autoCapitalize="none"
                        onChangeText={(val) => textInputChange(val, setUserID, setIsValidUser)}
                    />
                </View>

                {isValidUser ?
                    <Animatable.View animation="fadeInLeft" duration={500}>
                        <Text >{'กรุณาใส่ UserID'}</Text>
                    </Animatable.View>
                    :
                    <Text ></Text>
                }

                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <FontAwesome
                        name="key"
                        color='#000'
                        size={20}
                        style={{
                            margin: 5,
                        }}
                    />
                    <TextInput
                        placeholder={'รหัสผ่าน'}
                        placeholderTextColor="#666666"
                        value={pass}
                        style={{
                            borderRadius: 5,
                            backgroundColor: '#999',
                            color: '#000',
                            width: '80%',
                        }}
                        autoCapitalize="none"
                        secureTextEntry={true}
                        onChangeText={(val) => textInputChange(val, setPass, setIsValidPass)}
                    />
                </View>

                {isValidPass ?
                    <Animatable.View animation="fadeInLeft" duration={500}>
                        <Text >{'กรุณาใส่ Password'}</Text>
                    </Animatable.View>
                    :
                    <Text></Text>
                }
                <Card
                    style={{ height: height * 0.05, width: '50%', alignSelf: 'center', ...shadow.shadow10, marginBottom: 10 }}
                >
                    <TouchableOpacity
                        style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                        onPress={
                            () => {
                                handleSign()
                            }
                        }
                    >
                        <Text style={{ textAlign: 'center' }} >เข้าสู่ระบบ</Text>
                    </TouchableOpacity>
                </Card>
        </Card>
        </View >
    );

}

export default SignIn