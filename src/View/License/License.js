import React, { useEffect, useState, useContext, } from 'react'
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
} from 'react-native'
// import { useSelector, useDispatch } from "react-redux"
import Spinner from 'react-native-loading-spinner-overlay/lib';
import { Card } from 'react-native-paper';
import { shadow } from '../../utils/shadow';

const { width, height } = Dimensions.get("window")

const License = (props) => {

    // const store = useSelector((state) => state)
    
    const [data, setData] = useState([])
    const [search, setSearch] = useState('')

    const textInputChange = (txt, setTxt) => {
        setTxt(txt)
    }

    return (
        <View>
            {/* <Spinner visible={store.utils.isLoading} textContent={"Processing..."} textStyle={{ color: '#000' }} /> */}
            <Text>License</Text>
            <Card
                style={{ height: height * 0.05, width: '50%', alignSelf: 'center', marginTop: 20, ...shadow.shadow10 }}
            >
                <TouchableOpacity
                    style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                    onPress={
                        () => {
                            console.log('License go to Home')
                            props.navigation.reset({
                                index: 0,
                                routes: [{ name: 'HomeStack', params: {page: 'License', value: 'Test from License'} }]
                            })
                        }
                    }
                >
                    <Text style={{ textAlign: 'center' }} >Go To Home</Text>
                </TouchableOpacity>
            </Card>
        </View>
    )

}

export default License