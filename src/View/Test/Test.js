import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import { Card } from 'react-native-paper';
import { shadow } from "../../utils/shadow";

const { width, height } = Dimensions.get("window")

const Test = (props) => {

    useEffect(
        () => {
            console.log('Home ', props)
        }, []
    )

    return (
        <View>
            <Text>
                Test
            </Text>
            <Card
                style={{ height: height * 0.05, width: '50%', alignSelf: 'center', ...shadow.shadow10 }}
            >
                <TouchableOpacity
                    style={{ width: '100%', height: '100%', justifyContent: 'center', }}
                    onPress={
                        () => {
                            props.navigation.navigate('TestPage2', { page: 'Test', value: 'Test from Test' })
                        }
                    }
                >
                    <Text style={{ textAlign: 'center' }} >Go To Page2</Text>
                </TouchableOpacity>
            </Card>
        </View>
    )

}

export default Test