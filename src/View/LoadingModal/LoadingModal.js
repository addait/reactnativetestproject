import React from 'react'
import {
  View,
  ActivityIndicator,
} from "react-native"
import Modal from 'react-native-modal'

const LoadingModal = (props) => {

    const loading = () => {
        return  (
            props.isVisible?
            <ActivityIndicator size="large" color="#00ff00" />
            :
            null
        )
    }
    
    return(
        <View>
            <Modal 
            isVisible={props.isVisible}
            >
                <View>
                {loading()}
                </View>
            </Modal>
        </View>
    )
    
}

export default LoadingModal

