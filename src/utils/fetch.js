import Axios from "axios";

const URL = 'http://10.32.0.84/reactnative_mst/api/'

const axiop = async (data/*{ url, method, data, auth='' }*/) => {
    // console.log('fetch url ', url)
    // console.log('fetch data ', data)
    const _url = `${URL}${data.url}`
    // console.warn(_url)
    const headers = {'Content-Type': 'application/json', 'authorization' : data.auth }
    const options = { url: _url, method: data.method, data: data.data, headers }
    console.log('fetch option ', options) 
    const _data = await Axios(options)
        .then(
            (res) => {
                console.log('fetch res ', res.data)
                if (res.data.status !== 'failed') {
                    // console.log('fetch if ', res.data.data)
                    return {
                        fetchCheck: true,
                        data: res.data.data,
                    };
                } else {
                    return {
                        fetchCheck: false,
                        data: data.message,
                    };
                }
            }
        ).catch((err) => {
            // console.log('fetch error ', err)
            return {
                fetchCheck: false,
                data: [],
            };
        })
        return _data
}

export {
    axiop
}
